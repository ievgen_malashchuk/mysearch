<?php

/**
 * @file
 * Displays the search results.
 *
 * Available variables:
 * - $header: Text information about the result of a search.
 * - $word: The search term.
 * - $list: Array of the links.
 *
 * @see template_preprocess_mysearch_results()
 */
?>
<div id="mysearch">
  <h2><?php print $header; ?></h2>
  <?php if (!empty($list)): ?>
    <ul>
      <?php foreach ($list as $link): ?>
          <li><?php print $link; ?></li>
      <?php endforeach; ?>
    </ul>
  <?php endif; ?>
</div>
