# README #

This README explains what changed, added, or removed and why.

### Requirements ###

* A direct url, such as http://domain.com/mysearch/example will search all nodes in a database for occurrences of the word “example”. Any word should be searchable using this URL structure.
* The results will be printed to a page in an unordered list with the title of the node and a link to the node’s Drupal page.
* The page should have a title explaining to the user where they have landed.
 
### What required steps were made? ###

0. Installed Drupal, "mysearch.module.php" file moved to the "/drupal/sites/all/modules/custom/mysearch" folder.
1. Renamed "mysearch.module.php" to the "mysearch.module".
2. Added "mysearch.info" file.
3. Changed "mysearch.module" file format CR/LF from PC to Unix.
4. Added file description of the MySearch module for documenting the code.
5. Added function t() to the titles and description.
6. Added title callback function, page should have a title explaining to the user where they have landed.
7. Fixed and improved "mysearch_searchpage()" page callback function:

* Added missed closing tag "ul" and fixed closing tag "li".
* Removed excessive space from the end of line.
* Added check_plain to the search world in order to escape HTML code.
* Used db_like function and corrected pattern of the query.
* In oder to improve performance were changed SQL query (made search by title only in the {node} table, because node_revisions renamed to node_revision and the body field moved to separate table) and also removed unnecessary calls of node_load.
* Removed unsafe debugging output.
* Used function l() for making correct links to the node's Drupal pages.

### What additional steps were made? ###
 
8. Implemented page callback for incorrect urls.
9. Added theme and styles.